package com.oose2017.mshu1.hareandhounds;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

public class Board {
    private String gameId, firstPlayer;
    private int[][] board;
    private Hashtable<String, Integer> config = new Hashtable<>();

    /**
     * Constructor: construct a board object to memorize the current board situation.
     * board rule: first row hare, remaining row hound, format: [x][y]
     * @param gameId the id of the game
     * @param firstPlayer the first player type
     */
    public Board(String gameId, String firstPlayer) {
        this.gameId = gameId;
        this.firstPlayer = firstPlayer;
        this.board = new int[][]{{4, 1}, {0, 1}, {1, 0}, {1, 2}};
        this.recordConfig(convert2dto1d(this.board[0][0],this.board[0][1]),
                convert2dto1d(this.board[1][0],this.board[1][1]),
                convert2dto1d(this.board[2][0],this.board[2][1]),
                convert2dto1d(this.board[3][0],this.board[3][1]));
    }

    private void recordConfig(int hare, int hound1, int hound2, int hound3) {
        int[] boardCount = new int[15];
        boardCount[hare] = 2;
        boardCount[hound1] = 1;
        boardCount[hound2] = 1;
        boardCount[hound3] = 1;
        String hashString = Arrays.toString(boardCount);
        if (this.config.containsKey(hashString)) {
            this.config.put(hashString, this.config.get(hashString)+1);
        } else {
           this.config.put(hashString, 1);
        }
    }

    private int convert2dto1d(int x, int y) {
        return x*3 + y;
    }

    /**
     * return board to the user: first row: hare, second - forth row: hound 1-3
     * @return return board
     */
    public int[][] getBoard() {
        return this.board;
    }

    /**
     * find a piece according to the x, y coordinates
     * @param x x coordinate
     * @param y y coordinate
     * @return -1 for nothing, 0 for hare, 1-3 for hounds1-3
     */

    public int findPiece(int x, int y) {
        for (int i = 0; i < 4; i++) {
            if (this.board[i][0] == x && this.board[i][1] == y) return i;
        }
        return -1;
    }

    /**
     * check board conditions to see if hare escaped
     * @return a boolean, true for has escaped, vice versa.
     */
    public boolean hasEscaped() {
        for(int i = 1; i < 4; i++) {
            // some hounds are left to it.
            if (this.board[i][0] < this.board[0][0]) return false;
        }
        return true;
    }

    /**
     * check board conditions to see if the hare is trapped
     * @return a boolean, true for trapped, vice versa.
     */
    public boolean isTrapped() {
        int harePos = convert2dto1d(this.board[0][0], this.board[0][1]);
        int hound1Pos = convert2dto1d(this.board[1][0], this.board[1][1]);
        int hound2Pos = convert2dto1d(this.board[2][0], this.board[2][1]);
        int hound3Pos = convert2dto1d(this.board[3][0], this.board[3][1]);

        // add a list to check positions;
        ArrayList<Integer> houndSet = new ArrayList<>();
        houndSet.add(hound1Pos);
        houndSet.add(hound2Pos);
        houndSet.add(hound3Pos);

        // three different winning conditions
        if (harePos == convert2dto1d(2,0)) {
            if (houndSet.contains(convert2dto1d(3, 0)) && houndSet.contains(convert2dto1d(1, 0))
                    && houndSet.contains(convert2dto1d(2, 1))) return true;
        }
        if (harePos == convert2dto1d(2,2)) {
            if (houndSet.contains(convert2dto1d(1, 2)) && houndSet.contains(convert2dto1d(3, 2))
                    && houndSet.contains(convert2dto1d(2, 1))) return true;
        }
        if (harePos == convert2dto1d(4,1)) {
            if (houndSet.contains(convert2dto1d(3, 0)) && houndSet.contains(convert2dto1d(3, 1))
                    && houndSet.contains(convert2dto1d(3, 2))) return true;
        }
        return false;
    }

    /**
     * Check to see if game is stalled
     * @return true for stalled, vice versa.
     */
    public boolean isStalled() {
        if (this.config.containsValue(3)) return true;
        return false;
    }

    /**
     * update board positions after a successful move has been made
     * @param fromX original x position
     * @param fromY original y position
     * @param toX target x position
     * @param toY target y position
     */
    public void makeMove(int fromX, int fromY,  int toX, int toY) {
        int piece = this.findPiece(fromX, fromY);
        this.board[piece][0] = toX;
        this.board[piece][1] = toY;
        this.recordConfig(convert2dto1d(this.board[0][0],this.board[0][1]),
                convert2dto1d(this.board[1][0],this.board[1][1]),
                convert2dto1d(this.board[2][0],this.board[2][1]),
                convert2dto1d(this.board[3][0],this.board[3][1]));

    }

    /**
     * return Player1's type
     * @return if Hound, return hound. Else return hare.
     */
    public String getFirstPlayerType() {
        return this.firstPlayer;
    }

    /**
     * return Player2's type
     * @return if player1 is hound, return hound. Else return hare.
     */
    public String getOtherPlayerType() {
        if(this.firstPlayer.equals("HOUND")){
            return "HARE";
        } else {
            return "HOUND";
        }
    }

    /**
     * return gameId of this board
     * @return gameId as string.
     */
    public String getGameId() {
        return this.gameId;
    }

}
