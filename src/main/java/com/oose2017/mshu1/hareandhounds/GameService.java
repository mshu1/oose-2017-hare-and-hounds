//-------------------------------------------------------------------------------------------------------------//
// Code based on a tutorial by Shekhar Gulati of SparkJava at
// https://blog.openshift.com/developing-single-page-web-applications-using-java-8-spark-mongodb-and-angularjs/
//-------------------------------------------------------------------------------------------------------------//

package com.oose2017.mshu1.hareandhounds;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

public class GameService {
    private List<Board> boards;
    private List<State> states;
    private final Logger logger = LoggerFactory.getLogger(GameService.class);

    /**
     * Construct the model with a pre-defined datasource. The current implementation
     * also ensures that the DB schema is created if necessary.
     *
     *
     */
    public GameService() throws GameServiceException {
        this.boards = new ArrayList<>();
        this.states = new ArrayList<>();

        // since no database is required.
    }

    /**
     * Fetch all pieces in the board
     *
     * @return List of all pieces
     */
    public List<Piece> getBoard(String gameId) throws GameServiceIdException {
        if (!this.isValidGameId(gameId)) {
            logger.error("INVALID_GAME_ID");
            throw new GameServiceIdException("INVALID_GAME_ID", null);
        }

        int[][] board = boards.get(Integer.parseInt(gameId)).getBoard();
        List<Piece> pieces = new ArrayList<>();

        pieces.add(new Piece(gameId, null, "HARE", board[0][0], board[0][1]));
        for(int i = 1; i < 4; i++) {
            pieces.add(new Piece(gameId, null, "HOUND", board[i][0], board[i][1]));
        }

        return pieces;
    }

    /** return the state of the game
     *
     * @param gameId game id
     * @return a State object
     */
    public State getState(String gameId) throws GameServiceIdException{
        if (!this.isValidGameId(gameId)) {
            logger.error("INVALID_GAME_ID");
            throw new GameServiceIdException("INVALID_GAME_ID", null);
        }

        return states.get(Integer.parseInt(gameId));
    }

    /**
     * create a new game
     * @param body message sent from the front end with game information
     * @return pieces containing board information
     * @throws GameServiceException
     */
    public Piece createNewGame(String body) throws GameServiceException {
        int gameId = boards.size();
        String pieceType = new Gson().fromJson(body, Piece.class).getPieceType();
        if (!pieceType.equals("HOUND") && !pieceType.equals("HARE")) {
            throw new GameServiceException(null, null);
        }
        this.boards.add(new Board(Integer.toString(gameId), pieceType));
        this.states.add(new State(Integer.toString(gameId)));

        return new Piece(Integer.toString(gameId), "1", pieceType, 0, 0);
    }

    /**
     * Allow Player2 to join the game
     * @param gameId gameId for the targeted game
     * @return Piece including information of this target game
     * @throws GameServiceException
     * @throws GameServiceIdException
     * @throws GameServiceJoinException
     */
    public Piece joinGame(String gameId) throws GameServiceException, GameServiceIdException, GameServiceJoinException {
        if (!this.isValidGameId(gameId)) {
            logger.error("INVALID_GAME_ID");
            throw new GameServiceIdException("INVALID_GAME_ID", null);
        }
        if (!states.get(Integer.parseInt(gameId)).getState().equals("WAITING_FOR_SECOND_PLAYER")) {
            logger.error("GAME_JOINED");
            throw new GameServiceJoinException("GAME_JOINED", null);
        }

        State state = this.states.get(Integer.parseInt(gameId));
        Board board = this.boards.get(Integer.parseInt(gameId));
        String playerType = board.getOtherPlayerType();

        state.setTurn_Hound();

        return new Piece(gameId, "2", playerType, 0, 0);
    }

    /**
     * Handing move request sent by the front end; move pieces from board
     * @param gameId targeted game
     * @param body information about this move
     * @throws GameServiceException
     * @throws GameServiceIdException
     * @throws GameServiceMoveException
     */
    public void makeMove(String gameId, String body) throws GameServiceException, GameServiceIdException, GameServiceMoveException {
        // check for game Id
        if (!this.isValidGameId(gameId)) {
            logger.error("INVALID_GAME_ID");
            throw new GameServiceIdException("INVALID_GAME_ID", null);
        }

        Board board = boards.get(Integer.parseInt(gameId));
        State state = states.get(Integer.parseInt(gameId));
        Move move_request = new Gson().fromJson(body, Move.class);

        // check for player Id
        if (!this.isValidPlayerId(move_request.getPlayerId())) {
            logger.error("INVALID_PLAYER_ID");
            throw new GameServiceIdException("INVALID_PLAYER_ID", null);
        }

        String stateMsg = state.getState();
        if (stateMsg.equals("WIN_HOUND") || stateMsg.equals("WIN_HARE_BY_ESCAPE") || stateMsg.equals("WIN_HARE_BY_STALLING")){
            logger.error("INCORRECT_TURN");
            throw new GameServiceMoveException("INCORRECT_TURN", null);
        }

        /**
        if (!isValidMove(gameId, body)) {
            //not a valid move
            logger.error("ILLEGAL_MOVE");
            throw new GameServiceMoveException("ILLEGAL_MOVE", null);
        } else if (!isValidTurn(gameId, body)) {
            //not a valid turn
            logger.error("INCORRECT_TURN");
            throw new GameServiceMoveException("INCORRECT_TURN", null);
        } else {
            int currentType = board.findPiece(move_request.getFromX(),move_request.getFromY());
            if (currentType == 0) state.setTurn_Hound();
            else state.setTurn_Hare();

            // make the Move
            board.makeMove(move_request.getFromX(), move_request.getFromY(), move_request.getToX(), move_request.getToY());

            if (board.isStalled()) state.setWin_HareStalling();
            else if (board.isTrapped()) state.setWin_Hound();
            else if (board.hasEscaped()) state.setWin_HareEscape();
        }
         **/

        if (!isValidTurn(gameId, body)) {
            //not a valid turn
            logger.error("INCORRECT_TURN");
            throw new GameServiceMoveException("INCORRECT_TURN", null);
        } else if (!isValidMove(gameId, body)) {
            //not a valid move
            logger.error("ILLEGAL_MOVE");
            throw new GameServiceMoveException("ILLEGAL_MOVE", null);
        } else {
            int currentType = board.findPiece(move_request.getFromX(),move_request.getFromY());
            if (currentType == 0) state.setTurn_Hound();
            else state.setTurn_Hare();

            // make the Move
            board.makeMove(move_request.getFromX(), move_request.getFromY(), move_request.getToX(), move_request.getToY());

            if (board.isStalled()) state.setWin_HareStalling();
            else if (board.isTrapped()) state.setWin_Hound();
            else if (board.hasEscaped()) state.setWin_HareEscape();
        }

    }

    private boolean isValidTurn(String gameId, String body) {
        Board board = this.boards.get(Integer.parseInt(gameId));
        State state = this.states.get(Integer.parseInt(gameId));
        Move move_request = new Gson().fromJson(body, Move.class);

        //check if piece exist/obtain piece.
        int targetPiece = board.findPiece(move_request.getFromX(), move_request.getFromY());
        String houndPlayer;
        if (board.getFirstPlayerType().equals("HOUND")) houndPlayer = "1";
        else houndPlayer = "2";
        if (state.getState().equals("TURN_HOUND") && move_request.getPlayerId().equals(houndPlayer)) return true;
        if (state.getState().equals("TURN_HARE") && !move_request.getPlayerId().equals(houndPlayer)) return true;
        return false;
    }

    private boolean isValidMove(String gameId, String body) {
        Board board = boards.get(Integer.parseInt(gameId));
        State state = this.states.get(Integer.parseInt(gameId));
        Move move_request = new Gson().fromJson(body, Move.class);

        int destinationStatus = board.findPiece(move_request.getToX(), move_request.getToY());
        int targetPieceType = board.findPiece(move_request.getFromX(), move_request.getFromY());
        // position does not exist on board
        if (move_request.getToX() > 4 || move_request.getToX() < 0) return false;
        if (move_request.getToY() > 2 || move_request.getToY() < 0) return false;
        if ((move_request.getToX() == 0 || move_request.getToX() == 4 )&& (move_request.getToY() == 0 || move_request.getToY() == 2)) return false;

        // piece position taken
        if (targetPieceType == -1) return false;
        if (destinationStatus != -1) return false;

        // taking other player's piece
        if (state.getState().equals("TURN_HOUND") && targetPieceType == 0) return false;
        if (state.getState().equals("TURN_HARE") && targetPieceType > 0) return false;

        // wolf cannot go back
        if (targetPieceType > 0) {
            if (move_request.getFromX() > move_request.getToX()) return false;
        }

        // check if movement is actually 1.
        if (Math.abs(move_request.getFromX() - move_request.getToX()) > 1
                || Math.abs(move_request.getFromY() - move_request.getToY()) > 1) return false;

        // 4 diagonals prohibited check
        if ((move_request.getFromX() == 3 || move_request.getFromX() == 1 )
                && move_request.getFromY() == 1 && move_request.getToX() == 2) {
            if (move_request.getToY() == 0 || move_request.getToY() == 2) return false;
        }

        return true;

    }

    private boolean isValidGameId(String gameId) {
        try{
            int numId = Integer.parseInt(gameId);
            if (numId >= states.size() || numId < 0) return false;
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private  boolean isValidPlayerId(String playerId) {
        if (!playerId.equals("1") && !playerId.equals("2")){
            return false;
        }
        return true;
    }





    //-----------------------------------------------------------------------------//
    // Helper Classes and Methods
    //-----------------------------------------------------------------------------//

    public static class GameServiceException extends Exception {
        public GameServiceException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class GameServiceIdException extends Exception {
        public GameServiceIdException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class GameServiceJoinException extends Exception {
        public GameServiceJoinException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class GameServiceMoveException extends Exception {
        public GameServiceMoveException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
