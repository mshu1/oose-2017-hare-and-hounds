package com.oose2017.mshu1.hareandhounds;

public class Piece {
    private String gameId, pieceType, playerId;
    private int x, y;

    /**
     * This is the constructor to create game pieces
     * @param gameId the game id associated with this piece
     * @param playerId player id with this piece
     * @param pieceType the type of the piece: HOUND/HARE
     * @param x x-coor of this piece
     * @param y y-coor of this piece
     *
     */

    public Piece(String gameId, String playerId, String pieceType, int x, int y) {
        this.gameId = gameId;
        this.playerId = playerId;
        this.pieceType = pieceType;
        this.x = x;
        this.y = y;
    }

    /**
     * return game Id of this piece
     * @return game Id
     */
    public String getGameId(){
        return this.gameId;
    }

    /**
     * return player Id of this piece
     * @return player Id
     */
    public String getPlayerId(){
        return this.playerId;
    }

    /**
     * return the type of this piece
     * @return piece type
     */
    public String getPieceType(){
        return pieceType;
    }

    /**
     * return x coordinate of this piece
     * @return x coordinate
     */
    public int getX(){
        return this.x;
    }

    /**
     * return y coordinate of this piece
     * @return y coordinate
     */
    public int getY(){
        return this.y;
    }

    /**
     * set game id of this piece
     * @param gameId
     */
    public void setGameId(String gameId){
        this.gameId = gameId;
    }

    /**
     * set player id of this piece
     * @param playerId
     */
    public void setPlayerId(String playerId){
        this.playerId = playerId;
    }

    /**
     * set type of this piece
     * @param pieceType
     */
    public void setPieceType(String pieceType){
        this.pieceType = pieceType;
    }

    /**
     * set x coordinate of this piece
     * @param x
     */
    public void setX(int x){
        this.x = x;
    }

    /**
     * set y coordinate of this piece
     * @param y
     */
    public void setY(int y){
        this.y = y;
    }

    @Override
    public String toString() {
        return "Piece {" +
                "gameId='" + gameId + '\'' +
                ", playerId='" + playerId + '\'' +
                ", pieceType='" + pieceType + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

}

