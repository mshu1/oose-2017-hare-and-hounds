package com.oose2017.mshu1.hareandhounds;

public class FaultMessage {
    private String reason;

    /**
     * Constructor: message for faults sending to the server
     * @param reason
     */
    public FaultMessage(String reason) {
        this.reason = reason;
    }
}
