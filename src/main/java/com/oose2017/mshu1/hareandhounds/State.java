package com.oose2017.mshu1.hareandhounds;

public class State {
    private String state;
    private String gameId;

    /**
     * Constructor: state; recording the current state of the game
     * @param gameId
     */
    public State(String gameId) {
        this.state = "WAITING_FOR_SECOND_PLAYER";
        this.gameId = gameId;
    }

    /**
     * return current state of the game
     * @return current state
     */
    public String getState() {
        return this.state;
    }

    /**
     * return game Id of this state
     * @return game Id
     */
    public String getGameId() {
        return this.gameId;
    }

    /**
     * set state to hound's turn
     */
    public void setTurn_Hound() {
        this.state = "TURN_HOUND";
    }

    /**
     * set state to hare's turn
     */
    public void setTurn_Hare() {
        this.state = "TURN_HARE";
    }

    /**
     * set state to hound winning
     */
    public void setWin_Hound(){
        this.state = "WIN_HOUND";
    }

    /**
     * set state to hare winning by escaping
     */
    public void setWin_HareEscape(){
        this.state = "WIN_HARE_BY_ESCAPE";
    }

    /**
     * set state to hare winning by stalling
     */
    public void setWin_HareStalling(){
        this.state = "WIN_HARE_BY_STALLING";
    }


}
