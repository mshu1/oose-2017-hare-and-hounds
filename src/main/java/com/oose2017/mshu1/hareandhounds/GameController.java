//-------------------------------------------------------------------------------------------------------------//
// Code based on a tutorial by Shekhar Gulati of SparkJava at
// https://blog.openshift.com/developing-single-page-web-applications-using-java-8-spark-mongodb-and-angularjs/
//-------------------------------------------------------------------------------------------------------------//

package com.oose2017.mshu1.hareandhounds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

import static spark.Spark.*;

public class GameController {

    private static final String API_CONTEXT = "/api/v1";

    private final GameService gameService;

    private final Logger logger = LoggerFactory.getLogger(GameController.class);

    /**
     * Constructor for Game controller: communication to the front end
     * @param gameService service game controller needs to operate
     */
    public GameController(GameService gameService) {
        this.gameService = gameService;
        setupEndpoints();
    }

    private void setupEndpoints() {
        //fetch board
        get("/hareandhounds/api/games/:id" + "/board", "application/json", (request, response) -> {
            try {
                response.status(200);
                return gameService.getBoard(request.params(":id"));
            } catch (GameService.GameServiceIdException e) {
                response.status(404);
                return Collections.EMPTY_MAP;
            }
        }, new JsonTransformer());

        //fetch state
        get("/hareandhounds/api/games/:id" + "/state", "application/json", (request, response) -> {
            try {
                response.status(200);
                return gameService.getState(request.params(":id"));
            } catch (GameService.GameServiceIdException e) {
                response.status(404);
                return Collections.EMPTY_MAP;
            }
        }, new JsonTransformer());


        //Start a new game.
        post("/hareandhounds/api/games", "application/json", (request, response) -> {
            try {
                response.status(201);
                return gameService.createNewGame(request.body());
            } catch (Exception e) {
                response.status(400);
                logger.error("Unable to create a new game.");
                return Collections.EMPTY_MAP;
            }
        }, new JsonTransformer());

        //Join a game
        put("/hareandhounds/api/games/:id", "application/json", (request, response) -> {
            try {
                response.status(200);
                return gameService.joinGame(request.params(":id"));
            } catch (GameService.GameServiceIdException e) {
                response.status(404);
            } catch (GameService.GameServiceJoinException e) {
                response.status(410);
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());


        //Move
        post("/hareandhounds/api/games/:id/turns", "application/json", (request, response) -> {
            try {
                //decide which game board by ID
                response.status(200);
                gameService.makeMove(request.params(":id"), request.body());

            } catch (GameService.GameServiceIdException e){
                response.status(404);
                return new FaultMessage(e.getMessage());
            } catch (GameService.GameServiceMoveException e) {
                response.status(422);
                return new FaultMessage(e.getMessage());
            }
            return Collections.EMPTY_MAP;
        }, new JsonTransformer());
    }
}
