package com.oose2017.mshu1.hareandhounds;

public class Move {

    private String gameId, playerId;
    private int fromX, fromY, toX, toY;

    /**
     * Constructor: a class to handle move request sent from the front end.
     * @param gameId game Id of this game
     * @param playerId player who sent this request
     * @param fromX original x coordinate
     * @param fromY original y coordinate
     * @param toX targeted x coordinate
     * @param toY targeted y coordinate
     */
    public Move(String gameId, String playerId, int fromX, int fromY, int toX, int toY) {

        this.gameId = gameId;
        this.playerId = playerId;
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
    }

    /**
     * return game Id of this move
     * @return game Id
     */
    public String getGameId() {
        return gameId;
    }

    /**
     * return player Id of this move
     * @return player Id
     */
    public String getPlayerId() {
        return playerId;
    }

    /**
     * return original x coordinate of this move
     * @return original x coordinate
     */
    public int getFromX() {
        return fromX;
    }

    /**
     * return original y coordinate of this move
     * @return original y coordinate
     */
    public int getFromY() {
        return fromY;
    }

    /**
     * return target x coordinate of this move
     * @return target x coordinate
     */
    public int getToX() {
        return toX;
    }

    /**
     * return target y coordinate of this move
     * @return target t coordinate
     */
    public int getToY() {
        return toY;
    }

    @Override
    public String toString() {
        return "Piece {" +
                "gameId='" + gameId + '\'' +
                ", playerId='" + playerId + '\'' +
                ", fromX='" + fromX + '\'' +
                ", fromY='" + fromY + '\'' +
                ", toX='" + toX + '\'' +
                ", toY='" + toY + '\'' +
                '}';
    }
}
